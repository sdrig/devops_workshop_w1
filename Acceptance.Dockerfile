FROM node:10-alpine
# Install Chromium
RUN apk update && apk upgrade && \
    echo @edge http://nl.alpinelinux.org/alpine/edge/community >> /etc/apk/repositories && \
    echo @edge http://nl.alpinelinux.org/alpine/edge/main >> /etc/apk/repositories && \
    apk add --no-cache \
      git \
      tini \
      chromium@edge \
      nss@edge \
      freetype@edge \
      harfbuzz@edge \
      ttf-freefont@edge \
      libstdc++@edge
# Install taiko
ENV NPM_CONFIG_PREFIX /home/node/.npm-global
ENV PATH $PATH:/home/node/.npm-global/bin

ENV TAIKO_SKIP_CHROMIUM_DOWNLOAD true
ENV TAIKO_BROWSER_PATH /usr/bin/chromium-browser

RUN npm install @getgauge/cli -g --allow-root --unsafe-perm=true && gauge install js && \
    gauge install html-report && \
    gauge install screenshot && npm install --save-dev chai
RUN npm install taiko -g --allow-root --unsafe-perm=true


# COPY uitests/package*.json uitests/package.json
COPY uitests uitests
RUN ls -la /uitests/*
# RUN cd uitests  && npm install
# Copy scripts
WORKDIR /uitests
RUN npm install
RUN ls -la 
# WORKDIR /taiko

ENTRYPOINT ["npm", "run"]

CMD ["test"]